#include <iostream> 
#include <array> 
#include <exception> 
#include <vector> 
  
using namespace std; 
  
  
    struct SetException : public std::exception 
{ 
    SetException(char* x) : str(x)  {} 
  
   char* str; 
   const char* what() const throw() { return this->str; } 
  
}; // used specifically to handle errors that happen inside the sets 
  
class IntegerSet 
{ 
private: 
    int *setPtr; // pointer for the dynamically allocated set of numbers that this class represents 
    int length;  // length above the dynamically allocated array above 
    string setdescription;  
  
  
public: 
  
    // this is the max size a set can be to avoid a heap error 
    static const int MAXSETSIZE = 100000; 
    // constructors and destructor 
                        IntegerSet(int length); 
                        IntegerSet(int length, int inputarray[], int inputarraylength); // 1st param is the largest value set can hold, 2nd is a built in array with members present in this set 
    template <size_t N> IntegerSet(int length, array<int,N> (inputArray) ); // "   " .. 2nd is a c++11 array and makes a set out of it 
                        ~IntegerSet(); 
  
  
    // special functions I'm adding to make it more fun 
    // remember to srand(time(0)) in the client file before using this function 
        void     randomizeSet(double variableWeight);   // randomizes the contents of the set, based on the weighting of likelyhoood for each variable 
                                                // to be in the set. So if weight = .5, every variable has a .5 chance of being selected 
        int      getCardinality() const; 
        int      getLargestMember() const; 
        int      getSmallestMember() const; 
        double   getMean() const; 
        double   getMode() const; 
        void     clearAllInSet(); 
        int      getSumOfElements() const; 
        int      getSumOfElements(int start, int end) const; 
    vector<IntegerSet>   partitionAroundElement(int element) const; // partitions the set into two new sets around the 'element' variable 
    vector<IntegerSet>   partitionEvenOdd() const; 
  
    // inserting and deleting elements 
    bool insertElement(int key); 
    bool deleteElement(int key); 
    bool setSetDescription(string description) const; 
  
  
    // printing the set 
    void printSet() const; 
  
  
    // functions that compare and perform actions on other sets 
    // THESE FUNCTIONS THROW EXCEPTIONS OF TYPE MyExeption, ENCLOSE IN A TRY BLOCK 
    bool        isEqual(IntegerSet set2); 
    IntegerSet* unionOfSets(const IntegerSet &set2); // returns a pointer to a dynamically created 3rd integerset that is the union of this and set2 
    void        unionOfSets(const IntegerSet &set1, const IntegerSet &set2); // 'this' and object two have the union operator imposed on them and then stored in the IntegerSet 'newset' that is passed in by the user 
    IntegerSet *intersectionOfSets(const IntegerSet &set2);// returns a pointer to a dynamically created 3rd integerset that is the union of this and set2 
    void        intersectionOfSets(const IntegerSet &set1, const IntegerSet &set2);// 'this' and object two have the intersection operator imposed on them and then stored in the IntegerSet 'newset' that is passed in by the user 
  
  
    // get and set functions 
    // these are here for one set to get information about another set without having to allow direct array access 
    int  getLength() const; 
    bool doesElementExist(int key) const; 
  
  
};