#include <vector> 
#include <iostream>

#include "IntegerSet.h"

using namespace std;



// functions that aid the client  
//THESE ARE NOT REPLACING THE FUNCTIOINS INSIDE THE CLASS, THEY ARE SIMPLY MADE TO HELP KEEP THE main 
// FUNCTION FROM BECOMING TOO COMPLICATED. THESE FUNCTIONS BASICALLY DICTATE HOW THE USER INTERACTS WITH THE CLASS 
void randomizeSets(vector<IntegerSet> &setList); 
void createNewSet(vector<IntegerSet> &setList); 
void deleteSet(vector<IntegerSet> &setList); 
void intersectionOfSets(vector<IntegerSet> &setList); 
void unionOfSets(vector<IntegerSet> &setList); 
void areEqual(vector<IntegerSet> &setList); 
void otherOperations(vector<IntegerSet> &setList);






/// HELPER FUCNTIONS BELOW 
void randomizeSets(vector<IntegerSet> &setList) 
{ 
    system("cls"); 
    cout << "\n\n\n"; 
    cout << " In this section you can randomize any of the sets that currently exist\n"; 
    cout << " This works as follows:\n\n"; 
    cout << " You will enter which set you want to randomize,\n"; 
    cout << " and then enter what you want the probability of a member being included in the set is.\n\n"; 
  
    char menuchoice = 'Y'; 
    double probability = 0; 
    while(menuchoice != 'N') 
    { 
        int setChoice; // the set that they want to randomize 
        cout << "\n\n\n Which set would you like to randomize? " << setList.size() << " sets exist currently\n\n\n\n"; 
        do
        { 
            cin >> setChoice; 
        } 
        while(setChoice <= 0 || setChoice > setList.size()); 
          
        cout << " What would you like the probability of each element being included in the set?  0 <= p <= 1 \n"; 
        do
        { 
            cin >> probability; 
        } 
        while(probability < 0 || probability > 1); 
  
        setList[setChoice-1].randomizeSet(probability); 
  
        cout << "\n\n Would you like to randomize another set? Y or N\n"; 
        do
        { 
            cin >> menuchoice; 
        } 
        while(menuchoice != 'Y' && menuchoice != 'N'); 
        system("cls"); 
    } 
  
  
  
} 
void createNewSet(vector<IntegerSet> &setList) 
{ 
    system("cls"); 
  
      
    char createinputlist; // holds Y or N depending if they want to initialize or not 
      
      
        int setSize = 0; 
        cout << "\n\n \t\tCREATING NEW SET \n____________________________________________________\n\n\n\n"; 
        cout << " how large would you like this new set to be?   "; 
  
        // get the size of the new set 
        do 
        { 
            cin >> setSize; 
        } 
        while(setSize <= 0 || setSize >= IntegerSet::MAXSETSIZE); 
  
        cout << "\n\n Would you like to create a list of values to initialize your new set with?\n\t Y or N \n\n"; 
        // gets whether the user wants to initialize the values of their new set 
        do
        { 
            cin >> createinputlist; 
        } 
        while(createinputlist != 'Y' && createinputlist != 'N'); 
  
  
        // if they do want to initialize the set 
        if(createinputlist == 'Y') 
        { 
            vector<int> initializer; 
            cout << "\n\n Please enter the numbers you wish to initialize the new integerset with \n"; 
            cout << " ENTER -1 TO STOP ENTERING NUMBERS\n\n"; 
  
            int input = 0; 
  
            while(input != -1) 
            { 
                cin >> input; 
  
                if(input >= -1 && input < setSize) 
                { 
                    initializer.push_back(input); 
                      
                } 
                else
                { 
                    cout << "member out of range, must be greater than zero and less then " << setSize <<  "\n\n"; 
                } 
                  
            }// end while loop where user enters the values they want to initialize their new set with 
  
            //now we transfer the values from the vector that was used to take the input into a C++11 array class 
            // which can get passed to the IntegerSet constructor to initialize the values 
            int* newSetPtr = new int[setSize]; 
              
            for(int i = 0; i < initializer.size(); i++) 
            { 
                newSetPtr[i] = initializer[i]; 
            } 
  
            // this is the new set that the user has created and initialized with the entered values 
            IntegerSet newset(setSize, newSetPtr, initializer.size()); 
  
            delete [] newSetPtr; 
            newSetPtr = NULL; 
  
            setList.push_back(newset); // push the new set back onto our vector of integersets 
  
            cout << "\n\n\n New IntegerSet added to repository. The Number of this new set is " << setList.size() << "\n\n\n"; 
  
            system("pause"); 
  
        }// end if statement that determines if the user wants to create a list of values to initialize the integerset with 
  
        else // in case the user doesn't want to initialize the integerset with an array of values 
        { 
            IntegerSet newset(setSize); 
            setList.push_back(newset); 
            cout << "\n\n\n New IntegerSet added to repository. The Number of this new set is " << setList.size() << "\n\n\n"; 
  
            system("pause"); 
  
        } 
  
  
      
  
}// end function add new integerset 
void deleteSet(vector<IntegerSet> &setList) 
{ 
	system("cls");

	char doublecheck;
  cout << "\n\n\n \t\t REMOVING A SET\n\____________________________________________\n\n\n";
  cout << " Are you sure you want to move a set from the program? Y or N\n\n";
  do
  {
	  cin >> doublecheck;
  }
  while(doublecheck != 'Y' && doublecheck != 'N');

  if(doublecheck == 'Y')
  {
	  int numberofset;
	  cout << "\n\n\n Which set would you like to delete? There are " << setList.size() << "  sets currently\n\n";
	  do
	  {
		  cin >> numberofset;
	  }
	  while(numberofset < 0 || numberofset > setList.size());

	  setList.erase(setList.begin() + numberofset-1);

	  cout << "\n\n You have successfully removed set number " << numberofset << " from memory\n\n\n\n\n";

	  system("pause");


  }// end if making sure the user wants to delete a set

} // end delete set function
void intersectionOfSets(vector<IntegerSet> &setList)
{
	system("cls");
	int setA, setB;

	cout << "\n\n\n \t\tINTERSECTION OF SETS \n______________________________________________\n\n";

	cout << " You will now choose two sets for which we\n will perform the intersection operator on\n\n\n";
	cout << " This operation will result in a new set,\n which will be stored in set # " << setList.size()+1 << "\n\n\n";

	cout << " What is the first set you wish to be operated on? " << setList.size() << " current sets exist \n";

	do
	{
		cin >> setA;
	}
	while(setA < 1 || setA > setList.size());


	cout << "\n\n\n What is the second set you wish to be operated on? " << setList.size() << " current sets exist \n";

	do
	{
		cin >> setB;
	}
	while(setB < 1 || setB > setList.size());


	if(setList[setA-1].getLength() == setList[setB-1].getLength())
	{
	cout << " The Intersection operator on set " << setA << " and set " << setB << " has resulted \n";
	cout << " a new set being added to this program as set # " << setList.size()+1 << "\n\n";

	// declare a new set, and perform the intersection operator of setA and setB to the new set, setC
	IntegerSet setC(setList[setA-1].getLength());
	setC.intersectionOfSets(setList[setA-1], setList[setB-1]);

	// store setC in our vector
	setList.push_back(setC);


	}
	else
	{
		cout << "\n\n\n\n The capacities of your two sets must be equal in order to perform this operator!\n\n";
		cout << " Set # " << setA << " has capacity " << setList[setA-1].getLength() << "\n and Set # " << setB << " has capacity " << setList[setB-1].getLength() << "\n\n\n";
	}

	system("pause");

}
void unionOfSets(vector<IntegerSet> &setList)
{
	system("cls");
	int setA, setB;

	cout << "\n\n\n \t\tUNION OF SETS \n______________________________________________\n\n";

	cout << " You will now choose two sets for which\n we will perform the union operator on\n\n\n";
	cout << " This operation will result in a new set,\n which will be stored in set # " << setList.size()+1 << "\n\n\n";

	cout << " What is the first set you wish to be operated on? " << setList.size() << " current sets exist \n";

	do
	{
		cin >> setA;
	}
	while(setA < 1 || setA > setList.size());


	cout << "\n\n\n What is the second set you wish to be operated on? " << setList.size() << " current sets exist \n";

	do
	{
		cin >> setB;
	}
	while(setB < 1 || setB > setList.size());


	if(setList[setA-1].getLength() == setList[setB-1].getLength())
	{
	cout << " The Union operator on set " << setA << " and set " << setB << " has resulted \n";
	cout << " a new set being added to this program as set # " << setList.size()+1 << "\n\n";

	
	// declare a new set, and perform the intersection operator of setA and setB to the new set, setC
	IntegerSet setC(setList[setA-1].getLength());
	setC.unionOfSets(setList[setA-1], setList[setB-1]);

	// store setC in our vector
	setList.push_back(setC);


	}
	else
	{
		cout << "\n\n\n\n The capacities of your two sets must be equal in order to perform this operator!\n\n";
		cout << " Set # " << setA << " has capacity " << setList[setA-1].getLength() << "\n and Set # " << setB << " has capacity " << setList[setB-1].getLength() << "\n\n\n";
	}


	system("pause");

}
void areEqual(vector<IntegerSet> &setList); 
void otherOperations(vector<IntegerSet> &setList); 