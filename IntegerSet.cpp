#include <iostream> 
#include "IntegerSet.h" 
#include <ctime> 
#include <stdlib.h> 
#include <array> 
  
  
  
  
// constructors and destructor 
                        IntegerSet:: IntegerSet(int length) 
    { 
        if(length > 0 && length < MAXSETSIZE) 
        { 
            this->length = length; 
        } 
        else{this->length = 100;} 
        setPtr = new int[this->length]; 
        for(int i = 0; i < this->length; i++) // set all member to zero 
        { 
            *(setPtr+i) = 0; 
        }    
    } 
                        IntegerSet:: IntegerSet(int length, int inputarray[], int inputarraylength) 
    { 
        if(length > 0 && length <MAXSETSIZE) 
        { 
            this->length = length; 
        } 
        else
        { 
            this->length = 1000; // if they pass in an invalid value, we make the set = 1000, a reasonable number 
        } 
        setPtr = new int[this->getLength()]; 
        for(int i = 0; i < inputarraylength; i++) 
        { 
            if(inputarray[i] >= 0 && inputarray[i] < this->getLength()) 
            { 
                this->insertElement(inputarray[i]); 
            } 
        } 
    } 
                        // this next constructor takes the max value of the set as param #1 and an array of values to put in the set as param #2.  
    // the template is needed to allow the user to pass a varying length std::array as input 
    template <size_t N> IntegerSet::IntegerSet(int length, array<int,N> (inputArray)) 
    { 
        if(length > 0 && length < MAXSETSIZE) 
        { 
            this->length = length; 
        } 
        else
        { 
            this->length = 1000; 
        } 
        setPtr = new int[this->getLength()]; 
  
    for(int i = 0; i < inputArray.length; i++) 
    { 
        if(inputArray[i] >= 0 && inputArray[i] < this->length) 
        { 
            this->insertElement(inputArray[i]); 
        } 
    } 
}  
                        IntegerSet:: ~IntegerSet() 
    { 
        if(!setPtr) 
        delete this->setPtr; 
    } 
  
  
// special functions I'm adding to make it more fun 
    void        IntegerSet:: randomizeSet(double variableWeight) 
{ 
    if(getCardinality()) // to randomize the set we first empty it of all members 
    { 
        clearAllInSet(); 
    } 
    if(variableWeight > 0 && variableWeight <= 1) 
    { 
        int probDivisor = int(1/variableWeight); 
  
        for(int i = 0; i < this->length; i++) 
        { 
            if(rand()%probDivisor == 1) // there is a 1/probDivisor chance of having the expression = 1; which gives the desired randomization 
                this->insertElement(i); 
        } 
    } 
} 
    int         IntegerSet:: getCardinality() const
    { 
        int counter = 0; 
        for(int i = 0; i < this->getLength(); i++) 
        { 
            if(this->doesElementExist(i)) 
                counter++; 
        } 
        return counter; 
    } // returns how many elements are in the set 
    int         IntegerSet:: getLargestMember() const
    { 
        for(int i = this->getLength(); i <= 0; i--) 
        { 
            if(this->doesElementExist(i)) 
                return i; 
        } 
  
        return -1; 
    } 
    int         IntegerSet:: getSmallestMember() const
    { 
        for(int i = 0; i < this->getLength();  i++) 
        { 
            if(this->doesElementExist(i)) 
                return i; 
        } 
  
        return -1; 
    } 
    double      IntegerSet:: getMean() const
    { 
        double total = 0; 
        for(int i = 0; i < this->getLength();i++) 
        { 
            if(this->doesElementExist(i)) 
                total += double(i); 
        } 
        double mean = double(total/this->getLength()); 
        return mean; 
    } 
    double      IntegerSet:: getMode() const
    { 
        // if there is an even number of members, we have to take the average of the middle two, which requires this block 
        if(!(this->getCardinality()%2)) 
        { 
              
            int *holdarray = new int[this->getCardinality()]; 
            //fills the dynamic array with the values that are in our set 
            for(int i = 0, j = 0; i < this->getLength();i++) 
            { 
                if(this->doesElementExist(i)) 
                { 
                    holdarray[j] = i; 
                } 
            } 
  
            for(int i = 0, j = 0; i < this->getCardinality() && j >= 0; i++, j--) 
            { 
                if(j == i+1) // if the indices are one apart, the mode is the median of those to values 
                { 
                    double mode = double(holdarray[i]+holdarray[j]/2.0); 
                    delete[] holdarray; 
                    return mode; 
                } 
            } 
          
        }// end if there is an even number of members 
        else
        { 
            int *holdarray = new int[this->getCardinality()]; 
            //fills the dynamic array with the values that are in our set 
            for(int i = 0, j = 0; i < this->getLength();i++) 
            { 
                if(this->doesElementExist(i)) 
                { 
                    holdarray[j] = i; 
                } 
            } 
  
            for(int i = 0, j = this->getCardinality(); i < this->getCardinality() && j >= 0; i++, j--) 
            { 
                if(j == i) // if the indices are one apart, the mode is the median of those to values 
                { 
                    double mode = double(holdarray[i]); 
                    delete[] holdarray; 
                    return mode; 
                } 
            } 
        }// end if there are an odd number of members 
  
    }// end function 
    void        IntegerSet:: clearAllInSet() 
    { 
        for(int i = 0; i < this->getLength(); i++) 
        { 
            if(doesElementExist(i)) 
                deleteElement(i); 
        } 
    } 
    int         IntegerSet:: getSumOfElements() const
    { 
        int sum = 0; 
        for(int i = 0; i < this->getLength();i++) 
        { 
            if(doesElementExist(i)) 
                sum += i; 
        } 
  
        return sum; 
    } 
    int         IntegerSet:: getSumOfElements(int start, int end) const
    { 
        int sum = 0; 
        if( (end < this->getLength()) && (start >= 0) ) 
        { 
            for(int i = start; i < end; i++) 
            { 
                if(doesElementExist(i)) 
                    sum += i; 
            } 
          
            return sum; 
        } 
        else 
        { 
            throw SetException("\nStart or End index is out of bounds!\n"); 
        } 
    } 
    vector<IntegerSet> IntegerSet:: partitionAroundElement(int element) const
    { 
  
        vector<IntegerSet> newSets( 2, IntegerSet( this->getLength() ) ); 
  
        for(int i = 0; i < this->getLength(); i++) 
        { 
            if(i <= element && doesElementExist(i)) 
                newSets[0].insertElement(i); 
            else if( i > element && doesElementExist(i)) 
                newSets[1].insertElement(i); 
        } 
  
        return newSets; 
    } 
    vector<IntegerSet> IntegerSet:: partitionEvenOdd() const // returns a vector containing two sets, the first holds the evens, the second holds the odds 
    { 
        vector<IntegerSet> newSets( 2, IntegerSet( this->getLength() ) ); 
  
        for(int i = 0; i < this->getLength(); i++) 
        { 
            if(i%2 && doesElementExist(i)) 
                newSets[0].insertElement(i); 
            else if( !( i%2 ) && doesElementExist(i)) 
                newSets[1].insertElement(i); 
        } 
  
        return newSets; 
    } 
      
  
// inserting and deleting elements 
    bool IntegerSet:: insertElement(int key) 
    { 
        if(key >= 0 && key < this->getLength()) 
        {  
            this->setPtr[key] = 1; 
            return true; 
        } 
        return false; 
    } 
    bool IntegerSet:: deleteElement(int key) 
    { 
        if(key >= 0 && key < this->getLength()) 
        { 
            setPtr[key] = 0; 
            return true; 
        } 
        return false; 
    } 
  
  
// print function 
    void IntegerSet:: printSet() const
    { 
        if(this->getCardinality()) // cardinality is the number of members, if it's greater than one this is true 
        { 
            cout << "\n\n { "; 
            for(int i = 0; i < this->length;i++) 
            { 
                if(this->doesElementExist(i)) 
                { 
                    cout << i << " , "; 
                } 
            } 
        cout << " }\n"; 
        } 
        else
        { 
            cout << "-----"; 
        } 
  
    } 
  
  
// functions that work on other sets 
    bool        IntegerSet:: isEqual(IntegerSet set2) 
    { 
          
        // this throws an error if the lengths are not the same 
        if( !(this->getLength() == set2.getLength()) ) 
        { 
            throw SetException("\nSet Lengths Are Not Equal!\n"); 
        } 
          
        for(int i = 0; i < this->length; i++) 
        { 
            if(this->doesElementExist(i) != set2.doesElementExist(i)) 
                return false; 
        } 
        // if all elements pass the above loop then they are equal 
        return true; 
    } 
    IntegerSet* IntegerSet:: unionOfSets(const IntegerSet &set2) 
{ 
        // the third set we will be returning 
        IntegerSet* newset = new IntegerSet(this->getLength());  
  
        // this throws an error if the lengths are not the same 
            if( !(this->getLength() == set2.getLength()) ) 
        { 
            throw SetException("\nSet Lengths Are Not Equal!\n"); 
              
        } 
  
        for(int i = 0; i < this->length; i++) 
        { 
            if(this->doesElementExist(i) || set2.doesElementExist(i)) 
                newset->insertElement(i); 
        } 
      
        return newset; 
} 
    void        IntegerSet:: unionOfSets(const IntegerSet &set1, const IntegerSet &set2) 
    { 
      
  
        // this throws an error if the lengths are not the same 
        if( !(set1.getLength() == set2.getLength()) ) 
        { 
            throw SetException("\nSet Lengths Are Not Equal!\n"); 
        } 
  
        // this throws an error if the lengths are not the same 
        if( !(this->getLength() == set1.getLength()) ) 
        { 
            throw SetException("\nSet recieving the results must\n be the same length as the two sets \n being operated on!\n"); 
        } 
  
        for(int i = 0; i < this->length; i++) 
        { 
            if(set1.doesElementExist(i) || set2.doesElementExist(i)) 
                this->insertElement(i); 
        } 
          
    } 
    IntegerSet* IntegerSet:: intersectionOfSets(const IntegerSet &set2) 
{ 
    // the third set we will be returning 
        IntegerSet *newset = new IntegerSet(this->getLength()); 
  
        // this throws an error if the lengths are not the same 
        if( !(this->getLength() == set2.getLength()) ) 
        { 
            throw SetException("Set Lengths Are Not Equal!"); 
        } 
  
        for(int i = 0; i < this->length; i++) 
        { 
            if(this->doesElementExist(i) && set2.doesElementExist(i)) 
                newset->insertElement(i); 
        } 
  
        return newset; 
} 
    void        IntegerSet:: intersectionOfSets(const IntegerSet &set1, const IntegerSet &set2) 
{ 
  
        // this throws an error if the lengths are not the same 
        if( !(set1.getLength() == set2.getLength())  ) 
        { 
            throw SetException("\nSet Lengths Are Not Equal!\n"); 
        } 
  
        for(int i = 0; i < this->length; i++) 
        { 
            if(set1.doesElementExist(i) && set2.doesElementExist(i)) 
                this->insertElement(i); 
        } 
  
} 
  
  
// set and get functions that safely allow one IntegerSet class to obtain/share information with other integerSets 
    int  IntegerSet:: getLength() const
    { 
        return this->length; 
    } 
    bool IntegerSet:: doesElementExist(int key) const 
    { 
        if(key < this->length && key >= 0) 
        { 
            if(this->setPtr[key]) 
                return true; 
            else{return false;} 
        } 
    } 