#include <iostream> 
#include <time.h> 


#include "WorkingWithSets.h"
  
using namespace std; 

  
int main() 
{ 
   const int NUMMENUOPTIONS = 7;
    srand(time(NULL));// seed for the random generator of the class 
	cout << "\n\n\n\t\t Welcome to the Integer Set Calculator\n\n";
	cout << " This program allows you to create, destroy, and manipulate sets of integers\n\n";
	cout << "		 Press any key to start the program!\n\n\n\n\n\n\n\n\n\n";
	system("pause");
	

    // main loop and menu is inside this try/catch scope 
    // because the IntegerSet function throws exceptions of it's own type 
    try
    { 
		   // this is a vector that will contain all the integersets that the user wants to operate on 
		vector<IntegerSet> vectorofsets; 

        // main program loop below! 
        int menuchoice = 0; 
        do
        { 
            system("cls"); 
			cout << "\n\n\n\n";
            cout << "Please choose from the following options\n\n"; 
            cout << " 1.) Randomize a Set\n"; 
            cout << " 2.) Create a New Set\n"; 
            cout << " 3.) Delete an Existing Set\n"; 
            cout << " 4.) Create Intersection of 2 Sets\n"; 
            cout << " 5.) Create  Union of 2 Sets\n"; 
            cout << " 6.) Check if Two Sets are Equal\n"; 
            cout << " 7.) Operations that work on specific sets"; 
            cout << " \n\n  ENTER 0 TO QUIT\n\n"; 
              
            do{cin >> menuchoice;} 
            while((menuchoice < 1 || menuchoice > NUMMENUOPTIONS) ); 
  
            switch(menuchoice) 
            { 
  

                case 1: 
                      
					// sends the user to a function that lets them choose how to randomize a set  
						if(vectorofsets.size())
						randomizeSets(vectorofsets); 
						else
						{
							cout << "there are no sets to randomize, try creating one first!\n\n\n";
							system("pause");
						}
  
					break;// end case 1 
  


  
                case(2): 
  
						// lets the users push some new integersets onto the vector of sets 
						createNewSet(vectorofsets); 
  
					break;// end case 2 
  
  


                case(3): 

						// send the user to a function that lets them remove a stored IntegerSet from memory 
						if(vectorofsets.size())
						deleteSet(vectorofsets); 
						else
						{
							cout << "there are no sets to remove, try creating one first!\n\n\n";
							system("pause");
						}
  
					break;// end case 3 
  
  
                case(4): 

						// send the user to a function that lets them remove a stored IntegerSet from memory 
						if(vectorofsets.size() >= 2)
						intersectionOfSets(vectorofsets); 
						else
						{
							cout << "There are not enough sets to perform this operator!\n\n";
							cout << " There are currently " << vectorofsets.size() << " number of sets in memory, \n";
							cout << " you need at least 2 to perform this operator.\n\n\n\n";
							system("pause");
						}
  
  
					break;// end case 4 
  
  
                case(5): 

							// send the user to a function that lets them remove a stored IntegerSet from memory 
						if(vectorofsets.size() >= 2)
						unionOfSets(vectorofsets); 
						else
						{
							cout << "There are not enough sets to perform this operator!\n\n";
							cout << " There are currently " << vectorofsets.size() << " number of sets in memory, \n";
							cout << " you need at least 2 to perform this operator.\n\n\n\n";
							system("pause");
						}
  
					break;// end case 5 
  
  
                case(6): 
  
					break;// end case 6 
  
  
                case(7): 
  
					break;// end case 7 
  
  
                case(0): 
  
					break;// end case quit program 
  
  
            } // end switch statement
              
  
  
        }  // end of main program loop 
        while(menuchoice != 'q' && menuchoice != 'Q'); 
          
    }// end of the try brackets 
  
    // this part will catch any exceptions of the type Set 
    catch(const SetException &e) 
    { 
        cerr << e.what(); 
    } 
      
      
      
      
  
    system("pause"); 
  
    return 0; 
} 
  
  
  
